package main

import "fmt"

func main() {
	var wordOne string
	var charOne string

	fmt.Print("Enter a word: ")
	fmt.Scanln(&wordOne)
	fmt.Print("Polindrome: ")
	checkPolindrome(wordOne)

	fmt.Print("\nEnter a char: ")
	fmt.Scanln(&charOne)

	fmt.Print("Char: ")
	checkAlphabet(charOne)
}

func checkPolindrome(str string) bool {
	for i := 0; i < len(str); i++ {
		j := len(str) - 1 - i
		if str[i] != str[j] {
			fmt.Println(false)
			return false
		}
	}
	fmt.Println(true)
	return true
}

func checkAlphabet(str string) bool {
	var hasil bool
	vocal := [5]string{"a", "i", "u", "e", "o"}

	for _, x := range vocal {
		if x == str {
			hasil = true
			break
		}
	}
	if hasil {
		fmt.Println("vokal")
	} else {
		fmt.Println("konsonan")
	}
	return hasil
}
