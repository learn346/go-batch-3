package latihan

import (
	"fmt"
	"strings"
)

func Hello() string {
	// return quote.Hello()
	return "hello world"
}

func checkPolindrome(str string) bool {
	for i := 0; i < len(str); i++ {
		j := len(str) - 1 - i
		if str[i] != str[j] {
			fmt.Println(false)
			return false
		}
	}
	fmt.Println(true)
	return true
}

func checkAlphabet(str string) bool {
	var hasil bool
	vocal := [5]string{"a", "i", "u", "e", "o"}

	if str == "" || len(str) > 1 {
		hasil = false
		fmt.Println("invalid input")
	} else {
		for _, x := range vocal {
			if x == strings.ToLower(str) {
				hasil = true
				fmt.Println("vokal")
				break
			} else {
				hasil = false
				fmt.Println("konsonan")
				break
			}
		}
	}
	return hasil
}
