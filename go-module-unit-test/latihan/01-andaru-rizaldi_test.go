package latihan

import (
	"testing"
)

func TestHello(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "hello world",
			want: "hello world",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Hello(); got != tt.want {
				t.Errorf("Hello() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkPolindrome(t *testing.T) {
	type args struct {
		word string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "test 01",
			args: args{
				word: "test",
			},
			want: false,
		},
		{
			name: "test 02",
			args: args{
				word: "oppo",
			},
			want: true,
		},
		{
			name: "test 03",
			args: args{
				word: "lol",
			},
			want: true,
		},
		{
			name: "test 04",
			args: args{
				word: "pwd",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkPolindrome(tt.args.word); got != tt.want {
				t.Errorf("checkPolindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_checkPolindrom(b *testing.B) {
	word := "test"
	char := "a"
	for i := 0; i < 1000; i++ {
		checkPolindrome(word)
		checkAlphabet(char)
	}
}

func Test_checkAlphabet(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "too many char",
			args: args{
				str: "ab",
			},
			want: false,
		},
		{
			name: "konsonan",
			args: args{
				str: "b",
			},
			want: false,
		},
		{
			name: "vokal",
			args: args{
				str: "a",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkAlphabet(tt.args.str); got != tt.want {
				t.Errorf("checkAlphabet() = %v, want %v", got, tt.want)
			}
		})
	}
}
